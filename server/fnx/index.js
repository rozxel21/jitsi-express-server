const User = require('../model/user.js');
const Fingerprint = require('../model/fingerprint.js');

const bcrypt = require('bcryptjs');

exports.encryptPassword = (password) => {
	return new Promise((resolve, reject) => {
		bcrypt.hash(password, 10, function(err, hash){
			if(err) { console.log(err); reject(err);}
			console.log(hash);
			resolve(hash);
		});
	});
}

exports.getUserPassword = function(username){
	return new Promise((resolve, reject) => {
		User.findOne({username: username})
	    .select('password').exec(function(err, user){
	    	if(err) reject(err);
	    	resolve(user.password);
	    });
	});
}

exports.getUserByUsername = function(username){
	console.log('enter getUserByUsername');
	return new Promise((resolve, reject) => {
		User.findOne({username: username}, function(err, user){
			if(err) reject(err);
			resolve(user);
		});
	});
}

exports.dispatchFingerprint = function(user, hash){
	return new Promise((resolve, reject) => {
		Fingerprint.updateMany({user: user, hash: hash, isActive: true},
			{$set: {isActive: false}}, { returnNewDocument: true }, function(err, result){
				if(err) reject(err);
				console.log(result);
				resolve(result);
			});
	});
}

exports.saveFingerprint = function(id, token, fingerprint){
	return new Promise((resolve, reject) => {
		let fp = new Fingerprint;
		fp.user = id;
		fp.token = token;
		fp.hash = fingerprint.hash
		fp.browser.family = fingerprint.components.useragent.browser.family;
		fp.browser.version = fingerprint.components.useragent.browser.version;
		fp.device.family = fingerprint.components.useragent.device.family;
		fp.device.version = fingerprint.components.useragent.device.version;
		fp.os.family = fingerprint.components.useragent.os.family;
		fp.os.major = fingerprint.components.useragent.os.major;
		fp.os.minor = fingerprint.components.useragent.os.minor;
		fp.geoip.country = fingerprint.components.geoip.country;
		fp.save(function(err, fp) {
			if(err)
				reject(err);
			resolve(fp);
		});
	});
}