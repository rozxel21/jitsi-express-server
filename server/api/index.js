const express = require('express');
const router = express.Router();

// model
const User = require('../model/user');

const fnx = require('../fnx');

const user_upsert = require('../model/crud_wrapper/user/user_upsert');

let ucfirst = require('ucfirst');

const jwt =  require('jsonwebtoken');
const passport = require('passport');
const crypto = require('crypto-js');

const { passport_secret, encrypt_key } = require('../config/config');
const authenticate = passport.authenticate('jwt', { session: false });

router.post('/register', async (req, res, next) => {
	try{
		console.log(req.body);

		let user = new User;
		user.firstname = ucfirst(req.body.firstname);
		user.lastname = ucfirst(req.body.lastname);
		user.email = req.body.email;
		user.age = req.body.age;
		user.username = req.body.username;
		user.password = await fnx.encryptPassword(req.body.password);

		user = await user_upsert.save(user);
		res.status(200).json({success: true, msg: user});

	}catch(err){
		res.status(500).json(err);
	}
});


router.post('/login', async (req, res, next) => {
	try{
		console.log(req.body);
		console.log('await getUserByUsername');
		let user = await fnx.getUserByUsername(req.body.username);
		console.log(user);
		console.log('end getUserByUsername');		
		if(user){
		console.log('await user.validPassword');
			if(!(await user.validPassword(req.body.username, req.body.password))){
				console.log('incorrect password');
				res.status(401).send({success: false, msg: 'Authentication failed. Incorrect password'});
				return;
			}
		console.log('end user.validPassword');

			if(!user.isActive){
				console.log('InActive User');
				res.status(401).send({success: false, msg: 'Deactivated User'});
				return;
			}

			var token = jwt.sign({user: user}, passport_secret, { expiresIn: '24h' });
			await fnx.dispatchFingerprint(user.id, req.fingerprint.hash);
			await fnx.saveFingerprint(user.id, token, req.fingerprint);
	        res.status(200).json({success: true, token: token});
		}else{
			res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
			return;
		}
	}catch(err){
		console.log(err);
	}
});


module.exports = router;