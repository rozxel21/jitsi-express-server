const User = require('../../user');

exports.save = async  (user) => {
	return new Promise( async (resolve, reject) => {
		try{
			let canSave = await user_canSave(user);
			
			if(canSave.status = false){
				reject(canSave.msg);
			}else{
				console.log(user);
				user.save(function(err, user_result){
					if(err) reject(err)
					resolve(user_result);
				});
			}
		}catch(err){
			console.log(err);
		}
	});
}

let user_canSave = (user) => {
	return new Promise((resolve, reject) => {
		User.find({username: user.username}, function(err, result){
			if(err) reject(err);
			if(result){
				resolve({status: false, msg: 'Username already exist!'});
			}else{
				User.find({email: user.email}, function(err1, result1){
					if(err1) reject(err1);

					if(result1){
						resolve({status: false, msg: 'Email Address already exist!'});
					}else{
						resolve({status: true});
					}
				});
			}
		});
	});
}

