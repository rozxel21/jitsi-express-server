const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;

const bcrypt = require('bcryptjs');

const userScheme = new Schema({
	firstname: { type: String, required: true },
	lastname: { type: String, required: true },
	email: { type: String, required: true, index: { unique: true } },
	username: { type: String, required: true, index: { unique: true } },
	password: { type: String, required: true, select: false },
	age: { type: Number, required: true },
	isActive: { type: Boolean, default: true },
	created_at: { type: Date, default: Date.now },
	updated_at: Date
});

userScheme.plugin(mongoosePaginate);

// checking if password is valid
userScheme.methods.validPassword = async (username, password) => {
    try{
    	let hash = await getUserPassword(username);  	
    	return bcrypt.compareSync(password, hash);
    }catch(err){
    	console.log(err);
    	return false;
    }
};

let getUserPassword = function(username){
	return new Promise((resolve, reject) => {
		console.log(username);
		User.findOne({username: username})
		.select('password').exec(function(err, user){
	    	if(err) reject(err);
	    	console.log(user);
	    	resolve(user.password);
	    });
	});
}

let User = mongoose.model('user', userScheme);
module.exports = User;