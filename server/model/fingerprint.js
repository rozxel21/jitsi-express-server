const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fingerprintScheme = new Schema({
	hash: { type: String, required: true },
	browser: {
		family: { type: String, required: true },
		version: { type: String, required: true } 
	},
	device: {
		family: { type: String, required: true },
		version: { type: String, required: true }
	},
	os: {
		family: { type: String, required: true },
		major: { type: String, required: true },
		minor: { type: String, required: true }
	},
	geoip: {
		country: { type: String }
	},
	user: { type: String, required: true },
	token: { type: String, required: true, index: { unique: true } },
	created_at: { type: Date, default: Date.now },
	isActive: { type: Boolean, required: true, default: true }
});

fingerprintScheme.index({ hash: 1, user: 1, token: 1 });

module.exports = mongoose.model('fingerprint', fingerprintScheme);