const dotenv = require('dotenv');

dotenv.config();

module.exports = {
	port: process.env.PORT || 5000,
	db_uri: process.env.DB_URI || 'mongodb+srv://jitsi-vecom:jitsi-vecom@jitsi-vecom-hhyi7.mongodb.net/jitsi-vecom?retryWrites=true&w=majority',
	passport_secret: process.env.PASSPORT_SECRET || 'ilovegunsnroses',
	encrypt_key: process.env.ENCRYPT_KEY || 'neo.armstrong.cyclone.jet.armstrong.cannon',
};