const express = require('express');
const app = express();

const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const User = require('../model/user.js');
const Fingerprint = require('../model/fingerprint');

const { passport_secret } = require('./config');

app.use(passport.initialize());

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = passport_secret;

passport.use(new JwtStrategy(opts, function(jwt_payload, done){
    console.log("=== JwtStrategy ===");
    User.findById(jwt_payload.id, function(err, user){
        if (err) return done(err, false);
        if (user) {
            done(null, user);
        } else {
            done(null, false);
        }
    });
}));

module.exports = app;