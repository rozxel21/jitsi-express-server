const express = require('express');
const app = express();

const { port } = require('./server/config/config');
const passportConfig = require('./server/config/passport-config');

// dependencies declarations
const cors = require('cors');
const bodyParser = require('body-parser');
const logger = require('morgan');
const fingerprint = require('express-fingerprint');

// set database
const mongodb = require('./server/db/mongo-db.js');

// server api
const api = require('./server/api');

// setting dependencies into express
app.use(cors());
app.use(logger('tiny'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({type: 'application/json'}));
app.use(passportConfig);
app.use(fingerprint());

// setting api into express
app.use('/api', api);

app.listen(port, () => console.log(`Server running on port ${port}`));